#!/bin/bash

CURR_DIR=${PWD}

for project in */;
do
    if [ "$project" != ".git" ]; then
        cd ./$project
        ./build.sh
        cd $CURR_DIR
    fi
done

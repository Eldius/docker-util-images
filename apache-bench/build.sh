#!/bin/bash

set -ex
# SET THE FOLLOWING VARIABLES
# docker hub username
USERNAME=eldius
# image name
IMAGE=apache-bench

docker build \
    --no-cache \
    -t $USERNAME/$IMAGE:latest .


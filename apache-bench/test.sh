#!/bin/bash

CONTAINER_NAME="ab-test"

docker kill $CONTAINER_NAME

docker rm $CONTAINER_NAME


./build.sh

#docker run -it --name $CONTAINER_NAME \
#    eldius/apache-bench:latest \
#    /bin/ash

docker run -it --name $CONTAINER_NAME \
    eldius/apache-bench:latest \
    -c 5 \
    -n 100 \
    'http://google.com/'
